import db from "@elioway/dbhell";
import elioMadeFlesh from "@elioway/express-flesh";
import { adons, boneEnvVarsLoader, ribs, spine } from "@elioway/bones";

const commandDir = process.cwd();
let CFG = boneEnvVarsLoader(true, "", commandDir);

CFG = { ...CFG, PORT: 5021 }

elioMadeFlesh(CFG, { ...ribs, ...spine, ...adons }, db).then((app) =>
  app.listen(CFG.PORT, () =>
    console.log(`elioFlesh express server listening on ${CFG.PORT}`)
  )
);
