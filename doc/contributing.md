# cms Contributors

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/eliofaithful.git
cd eliofaithful
git clone https://gitlab.com/eliofaithful/cms.git
```

## Dev Enviroment

```
virtualenv --python=python3 venv-builder
or
python3 -m venv venv-builder

source .env
# or
source .env.fish
pip install -r requirements/local.txt
```

### TODOS

1. TODOS
