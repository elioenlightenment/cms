![](https://elioway.gitlab.io/elioflesh/express-flesh/elio-flesh-express-logo.png)

# cms

A simple photo gallery with instructions for anyone to build something.

- [express-flesh Documentation](https://elioway.gitlab.io/elioflesh/express-flesh/)

## Seeing is Believing

```
set APPROOT ~/Dev/elioenlightenment/cms
set APPIDENTIFIER cms

cd $APPROOT
                    
rm -rf .data

# Home page
set IDENTIFIER $APPIDENTIFIER
bones takeupT $IDENTIFIER --mainEntityOfPage=WebPage 
bones anonifyT $IDENTIFIER
bones updateT $IDENTIFIER --name="CMS"
bones updateT $IDENTIFIER --alternateName="the elioWay"
bones updateT $IDENTIFIER --description="This could be a clock of HTML from filesystem, or hardcoded."
bones updateT $IDENTIFIER --disambiguatingDescription="A CMS system built out of Things"
bones updateT $IDENTIFIER --image=""
bones updateT $IDENTIFIER --name=""
bones updateT $IDENTIFIER --potentialAction=""
bones updateT $IDENTIFIER --sameAs=""
bones updateT $IDENTIFIER --subjectOf="elioWay"
bones updateT $IDENTIFIER --url="http://cms.theElioWay.net/"

bones updateT $IDENTIFIER --WebPage.breadcrumb=""
bones updateT $IDENTIFIER --WebPage.lastReviewed="2023-02-08",
bones updateT $IDENTIFIER --WebPage.mainContentOfPage=""
bones updateT $IDENTIFIER --WebPage.primaryImageOfPage=""
bones updateT $IDENTIFIER --WebPage.relatedLink=""
bones updateT $IDENTIFIER --WebPage.reviewedBy="Tim Bushell"
bones updateT $IDENTIFIER --WebPage.significantLink=""
bones updateT $IDENTIFIER --WebPage.significantLinks=""
bones updateT $IDENTIFIER --WebPage.speakable=""
bones updateT $IDENTIFIER --WebPage.specialty=""
bones cleanT $IDENTIFIER

for P in 1 2 3 4 5 6 7
    set IDENTIFIER page$P
    bones takeonT $APPIDENTIFIER $IDENTIFIER --mainEntityOfPage=WebPage
    bones updateT $IDENTIFIER --name=$IDENTIFIER
    bones anonifyT $IDENTIFIER
    bones cleanT $IDENTIFIER
    # Add a link to another gallery
    bones takeonT $APPIDENTIFIER "LINK2"$IDENTIFIER --mainEntityOfPage=EntryPoint --name=$IDENTIFIER --url=$IDENTIFIER

    # Add a link to back home
    bones takeonT $IDENTIFIER "LINK2"$APPIDENTIFIER --mainEntityOfPage=EntryPoint --name=$APPIDENTIFIER --url=$APPIDENTIFIER
end 
```

Run it

```
npm run serve
```

Do more! Start here.

- [express-flesh Quickstart](https://elioway.gitlab.io/elioflesh/express-flesh/quickstart.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioflesh/express-flesh/apple-touch-icon.png)
